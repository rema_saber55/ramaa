import 'package:flutter/material.dart';


void main() {
  runApp(
    Directionality(
      textDirection: TextDirection.ltr,
      child: Material(
          color: Colors.white,
          child: Column(
            mainAxisAlignment:MainAxisAlignment.center ,
            children: [
              const Text("Line 1"),
              const SizedBox(
                height: 32,
              ),
              const Text("Line 2"),
              const SizedBox(
                height: 32,
              ),
              const Text("Line 3"),
              const SizedBox(
                height: 32,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,

                 children: const [
                  const Text("line 4"),
                  const SizedBox(width: 32,),
                  const Text("line 5"),
                ],
              ),
            ],
          )
      ),
    ),
  );
}